## Установка
- php artisan migrate
- php artisan db:seed
- Добавить в крон:  * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
- php artisan storage:link
- php artisan key:generate
- Доступы к админке: admin/admin

## Библеотеки 
- [админка] (https://github.com/z-song/laravel-admin)
- [генератор моделей] (https://github.com/krlove/eloquent-model-generator)
- [изображения для админки] (https://github.com/nicklasos/laravel-admin-media-library)
- [изображения] (https://github.com/spatie/laravel-medialibrary)
- [для сео] (https://github.com/artesaos/seotools)

