<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/twitch/', 'TwitchStreamsController@list')->name('twitch.list');
Route::get('/twitch/{id}', 'TwitchStreamsController@detail')->name('twitch.detail');

Route::get('/news/', 'NewsController@list')->name('news.list');
Route::get('/news/{id}', 'NewsController@detail')->name('news.detail');

Route::get('/hero/', 'HeroController@list')->name('hero.list');
Route::get('/hero/{id}', 'HeroController@detail')->name('hero.detail');



