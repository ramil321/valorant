<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TwitchStreams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitch_streams', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('title');
            $table->string('type');
            $table->string('thumbnail_url');
            $table->string('language');
            $table->integer('viewers_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitch_streamers');
    }
}
