<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('name_en');
            $table->text('description');
        });

        Schema::create('hero_skill', function (Blueprint $table) {
            $table->bigInteger('hero_id')->unsigned();
            $table->bigInteger('skill_id')->unsigned();
            $table->foreign('hero_id')
                ->references('id')
                ->on('hero')
                ->onDelete('cascade');
            $table->foreign('skill_id')
                ->references('id')
                ->on('skill')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hero_skill');
        Schema::dropIfExists('skill');
    }
}
