<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->insert([
            [
                'parent_id' => 9,
                'order' => 0,
                'title' => 'Умения',
                'icon' => 'fa-resistance',
                'uri' => '/skill',
                'permission' => '*'
            ],
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Персонажи и Умения',
                'icon' => 'fa-github-square',
                'uri' => NULL,
                'permission' => '*'
            ],
            [
            'parent_id' => 9,
                'order' => 0,
                'title' => 'Персонажи',
                'icon' => 'fa-500px',
                'uri' => '/hero',
                'permission' => '*'
            ],
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Новости',
                'icon' => 'fa-newspaper-o',
                'uri' => '/news',
                'permission' => '*'
            ]
        ]);

    }
}
