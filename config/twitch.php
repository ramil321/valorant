<?php

return [
    'TWITCH_CLIENT_ID' => env('TWITCH_CLIENT_ID'),
    'TWITCH_GAME_ID' => env('TWITCH_GAME_ID'),
];
