<?php
/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => false,
            'titleBefore'  => false, // Put defaults.title before page title, like 'It's Over 9000! - Dashboard'
            'description'  => false,
            'separator'    => ' - ',
            'keywords'     => [],
            'canonical'    => false, // Set null for using Url::current(), set false to total remove
            'robots'       => false, // Set to 'all', 'none' or any combination of index/noindex and follow/nofollow
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null,
        ],

        'add_notranslate_class' => false,

        'pages' => [
            'home' => [
                'index' => [
                    'title' => "Valorant - обновления, новости, матчи, стримы, турниры, гайды, герои",
                    'description' => 'Крупнейший русский сайт по Valorant.'.
                        'Расписание матчей и турниров, актуальные новости, лучшие гайды, '.
                        'захватывающие стримы, база знаний героев и предметов и многое другое',
                ]
            ],
            'news' => [
                'list' => [
                    'title' => 'Новости Valorant',
                    'description' => 'Самые свежие новости Valorant:'.
                    ' статьи, интервью, обновления, турниры и многое другое',
                ]
            ],
            'hero' => [
                'list' => [
                    'title' => 'Персонажи из Valorant',
                    'description' => 'Умения персонажей из Valorant, скилы, описание скилов, описание умений.',
                ]
            ],
            'twitch' => [
                'list' => [
                    'title' => 'Стримы Valorant',
                    'description' => 'Стримы по Valorant, топ стримеры.',
                ]
            ]
        ],

    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => false, // set false to total remove
            'description' => false, // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
    'json-ld' => [
        /*
         * The default configurations to be used by the json-ld generator.
         */
        'defaults' => [
            'title'       => '', // set false to total remove
            'description' => '', // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => '',
            'images'      => [],
        ],
    ]
];
