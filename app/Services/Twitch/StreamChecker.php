<?php


namespace App\Services\Twitch;



use \Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;

class StreamChecker
{
    /**
     * @var string
     */
    const STREAMS_URL = 'https://api.twitch.tv/helix/streams';

    /**
     * @var string
     */
    private $clientId;
    /**
     * @var int
     */
    private $gameId;

    /**
     * StreamChecker constructor.
     */
    public function __construct()
    {
        $this->clientId = config('twitch.TWITCH_CLIENT_ID');
        $this->gameId = config('twitch.TWITCH_GAME_ID');
    }

    /**
     * @return array
     */
    public function getTopStreams(): array
    {
        $response = Http::withHeaders(['Client-ID' => $this->clientId])
            ->get(self::STREAMS_URL,
                [
                    'game_id' => $this->gameId,
                    'language' => 'ru',
                ]);
        ;
        return json_decode($response->body(), true);
    }

}
