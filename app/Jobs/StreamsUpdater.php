<?php

namespace App\Jobs;

use App\Models\Twitch\TwitchStreamers;
use App\Models\Twitch\TwitchStreams;
use App\Services\Twitch\StreamChecker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StreamsUpdater implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var StreamChecker
     */
    private $streamChecker;

    /**
     * Create a new job instance.
     *
     * @param StreamChecker $streamChecker
     */
    public function __construct(StreamChecker $streamChecker)
    {
        $this->streamChecker = $streamChecker;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $topStreams = $this->streamChecker->getTopStreams();
        if ( !isset($topStreams['data'])) {
            return;
        }

       $insertData = array_map(function ($item) {
            $thumbnailUrl = $item['thumbnail_url'];
            $thumbnailUrl = str_replace('{width}', '700', $thumbnailUrl);
            $thumbnailUrl = str_replace('{height}', '400', $thumbnailUrl);
            return [
                'user_id' => $item['user_id'],
                'user_name' => $item['user_name'],
                'title' => $item['title'],
                'type' => $item['type'],
                'thumbnail_url' => $thumbnailUrl,
                'language' => $item['language'],
                'viewers_count' => $item['viewer_count'],
            ];
        }, $topStreams['data']);

        TwitchStreams::truncate();
        TwitchStreams::insert($insertData); // Eloquent approach*/
    }
}
