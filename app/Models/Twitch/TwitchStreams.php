<?php

namespace App\Models\Twitch;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $user_id
 * @property string $user_name
 * @property string $title
 * @property string $type
 * @property string $thumbnail_url
 * @property string $language
 * @property int $viewers_count
 */
class TwitchStreams extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'user_name', 'title', 'type', 'thumbnail_url', 'language', 'viewers_count'];

}
