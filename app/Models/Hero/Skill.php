<?php

namespace App\Models\Hero;

use App\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

/**
 * @property integer $id
 * @property integer $hero_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $name_en
 * @property string $description
 * @property string $image
 * @property Hero $hero
 */
class Skill extends Model implements HasMedia
{
    use HasMediaTrait;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'skill';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'name',
        'name_en',
        'description',
        'image'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hero()
    {
        return $this->belongsTo('App\Models\Hero\Hero');
    }

    /**
     * @param Media|null $media
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100)
            ->nonQueued();
    }

    public function setImageAttribute()
    {
    }
}
