<?php


namespace App\Http\Controllers;


use App\Models\Hero\Hero;
use Artesaos\SEOTools\Traits\SEOTools;

class HeroController extends Controller
{
    use SEOTools;
    /**
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $heroesRandom = Hero::inRandomOrder()->limit(2)->get();
        $hero = Hero::findOrFail($id);
        $this->seo()->setTitle($hero->name);
        $this->seo()->setDescription($hero->preview_text);

        return view('frontend.pages.hero.detail', compact(
            [
                'heroesRandom',
                'hero',
            ]
        ));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $this->seo()->setTitle(config('seotools.meta.pages.hero.list.title'));
        $this->seo()->setDescription(config('seotools.meta.pages.hero.list.description'));
        $heroes = Hero::all();
        return view('frontend.pages.hero.list', compact(
            [
                'heroes'
            ]
        ));
    }
}
