<?php


namespace App\Http\Controllers;


use App\Models\Hero\Hero;
use App\Models\News\News;
use App\Models\Twitch\TwitchStreams;
use Artesaos\SEOTools\Traits\SEOTools;

class HomeController extends Controller
{
    use SEOTools;
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->seo()->setTitle(config('seotools.meta.pages.home.index.title'));
        $this->seo()->setDescription(config('seotools.meta.pages.home.index.description'));
        $twitchStreams = TwitchStreams::limit(4)->get();
        $news = News::limit(4)->get();
        $heroes = Hero::all();
        return view('frontend.index', compact(
            [
                'twitchStreams',
                'news',
                'heroes',
            ]
        ));
    }
}
