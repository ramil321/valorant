<?php


namespace App\Http\Controllers;

use App\Models\News\News;
use Artesaos\SEOTools\Traits\SEOTools;

class NewsController extends Controller
{
    use SEOTools;
    /**
     * @param integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $news = News::findOrFail($id);
        $this->seo()->setTitle($news->title);
        $this->seo()->setDescription($news->preview_text);
        return view('frontend.pages.news.detail', ['news' => $news]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $this->seo()->setTitle(config('seotools.meta.pages.news.list.title'));
        $this->seo()->setDescription(config('seotools.meta.pages.news.list.description'));
        $news = News::all();
        return view('frontend.pages.news.list', compact(
            [
                'news'
            ]
        ));
    }
}
