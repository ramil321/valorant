<?php


namespace App\Http\Controllers;


use App\Models\Twitch\TwitchStreams;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TwitchStreamsController extends Controller
{
    use SEOTools;
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id)
    {
        $stream = TwitchStreams::on()->where(['user_id' => $id])->first();
        $this->seo()->setTitle('Стрим '.$stream->user_name);
        $this->seo()->setDescription('Стрим '.$stream->user_name);

        if (! isset($stream)) {
            throw new ModelNotFoundException;
        }
        return view('frontend.pages.twitch.detail',
            [
                'stream' => $stream
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $this->seo()->setTitle(config('seotools.meta.pages.twitch.list.title'));
        $this->seo()->setDescription(config('seotools.meta.pages.twitch.list.description'));
        $streams = TwitchStreams::all();
        return view('frontend.pages.twitch.list', [
            'streams' => $streams
        ]);
    }
}
