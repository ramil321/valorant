<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Editor\EditorImages;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Dashboard')
            ->description('Description...')
            ->row(Dashboard::title())
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::dependencies());
                });
            });
    }

    /**
     * success response method.
     *
     * @param Request $request
     * @return void
     */
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {

            $editorImages = new EditorImages();
            $editorImages->addMedia($request->file('upload'))->toMediaCollection('editor');
            $editorImages->save();
            $url =  $editorImages->getFirstMediaUrl('editor');
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
}
