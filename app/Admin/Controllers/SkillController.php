<?php


namespace App\Admin\Controllers;


use App\Models\Hero\Skill;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SkillController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Skill());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name');
        $grid->column('name_en');
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Skill::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('image')->image();
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Skill);
        $form->display('id', __('ID'));
        $form->text('name')->rules('required|max:255');
        $form->text('name_en')->rules('required|max:255');
        $form->textarea('description')->rules('required');
        $form->mediaLibrary('image');
        return $form;
    }
}
