<?php


namespace App\Admin\Controllers;


use App\Models\Hero\Hero;
use App\Models\Hero\Skill;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class HeroController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Hero());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('name');
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Hero::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('image')->image();
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Hero());

        $form->display('id', __('ID'));
        $form->text('name')->rules('required|max:255');;
        $form->editor('preview_text')->rules('required');;
        $form->editor('detail_text')->rules('required');;
        $form->multipleSelect('skills')->options(Skill::all()->pluck('name', 'id'));
        $form->mediaLibrary('image');
        return $form;
    }
}
