<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index')->name('admin.home');
    $router->post('ckeditor/upload', 'HomeController@upload')->name('ckeditor.upload');
    $router->resource('/news', 'NewsController');
    $router->resource('/hero', 'HeroController');
    $router->resource('/skill', 'SkillController');
});
