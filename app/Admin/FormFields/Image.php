<?php


namespace App\Admin\FormFields;


use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image as InterventionImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image extends \Encore\Admin\Form\Field\Image
{

    /**
     * @var callable
     */
    public $thumbnailCallFunc;

    /**
     * @param callable $callable (\Intervention\Image\Image, $width, $height)
     */
    public function thumbnailCallable($callable)
    {
        $this->thumbnailCallFunc = $callable;
    }

    /**
     * Upload file and delete original thumbnail files.
     *
     * @param UploadedFile $file
     *
     * @return $this
     */
    protected function uploadAndDeleteOriginalThumbnail(UploadedFile $file)
    {
        foreach ($this->thumbnails as $name => $size) {
            // We need to get extension type ( .jpeg , .png ...)
            $ext = pathinfo($this->name, PATHINFO_EXTENSION);

            // We remove extension from file name so we can append thumbnail type
            $path = Str::replaceLast('.' . $ext, '', $this->name);

            // We merge original name + thumbnail name + extension
            $path = $path . '-' . $name . '.' . $ext;

            /** @var \Intervention\Image\Image $image */
            $image = InterventionImage::make($file);

            if (is_callable($this->thumbnailCallFunc)) {
                call_user_func($this->thumbnailCallFunc, $image , $size[0], $size[1]);
            } else {
                $action = $size[2] ?? 'resize';
                // Resize image with aspect ratio
                $image->$action($size[0], $size[1], function (Constraint $constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas($size[0], $size[1], 'center', false, '#ffffff');
            }

            if (!is_null($this->storagePermission)) {
                $this->storage->put("{$this->getDirectory()}/{$path}", $image->encode(), $this->storagePermission);
            } else {
                $this->storage->put("{$this->getDirectory()}/{$path}", $image->encode());
            }
        }
    }
}
