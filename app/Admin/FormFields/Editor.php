<?php


namespace App\Admin\FormFields;


use Encore\Admin\Form\Field;

class Editor extends Field
{
    protected static $js = [
        '//cdn.ckeditor.com/4.5.10/standard/ckeditor.js',
    ];

    public function render()
    {
        $this->script = " CKEDITOR.replace('{$this->id}', {
            filebrowserUploadUrl: '".route('ckeditor.upload', ['_token' => csrf_token() ])."'
        }); ";

        return parent::render();
    }
}
