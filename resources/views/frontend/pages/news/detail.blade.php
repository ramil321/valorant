<?php
/**
 * @var App\Models\News\News $news
 */
?>

@extends('frontend.layout')
@section('content')
    <div class="container content-black">
        <div class="row main-row mt-4">
            <div class="col-lg-8 col-md-12">
                <h1>{{ $news->title }}</h1>

                {!! $news->detail_text !!}

            </div>
            <div class="col-lg-4 col-md-12">

            </div>
        </div>
    </div>
@endsection
