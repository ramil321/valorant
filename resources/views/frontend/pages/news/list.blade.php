<?php
/**
 * @var App\Models\News\News[] $news
 */
?>
@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="main-row mt-4">
            <div class="row">
                @foreach($news as $key=>$item)
                    <div class="col-md-4">
                        <div class="main-card">
                            <a href="{{ route('news.detail', ['id' => $item->id]) }}">
                                <img class="card-img-top" src="{{ $item->getFirstMediaUrl('image') }}" alt="">
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="{{ route('news.detail', ['id' => $item->id]) }}"><h3>{{ $item->title }}</h3></a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    @if(($key + 1) % 3 == 0)
            </div>
            <div class="row">
                @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
