<?php
/**
 * @var App\Models\Hero\Hero[] $heroes
 */
?>

@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="main-row mt-4">
            <div class="row">
            @foreach($heroes as $key=>$item)
                <div class="col-md-3">
                    <div class="main-card hero">
                        <a href="{{ route('hero.detail', ['id' => $item->id]) }}">
                            <img class="card-img-top" src="{{ $item->getFirstMediaUrl('image') }}" alt="">
                        </a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{ route('hero.detail', ['id' => $item->id]) }}"><h3>{{ $item->name }}</h3></a>
                            </h4>
                        </div>
                    </div>
                </div>
                @if(($key + 1) % 4 == 0)
                </div>
                <div class="row">
                @endif
            @endforeach
            </div>
        </div>
    </div>
@endsection
