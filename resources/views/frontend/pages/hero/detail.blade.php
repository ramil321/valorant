<?php
/**
 * @var App\Models\Hero\Hero $hero
 * @var App\Models\Hero\Hero[] $heroesRandom
 *
 */
?>

@extends('frontend.layout')
@section('content')
    <div class="container content-black">
        <div class="row mt-4">
            <div class="col-sm-9">
                <div class="row main-row">
                    <div class="col-sm-3">


                        <div class="">
                            <img class="card-img-top" src="{{ $hero->getFirstMediaUrl('image') }}" alt="">
                            <div class="card-body">
                                @if($hero->skills)
                                    @foreach($hero->skills as $skill)
                                    <div>{{ $skill->name }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>



                    </div>
                    <div class="col-sm-9">

                        <div class="">
                            <h1>{{ $hero->name }}</h1>
                            <div class="card-body">
                                {!! $hero->detail_text !!}
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row main-row mt-2">
                    <div class="col-sm-12">

                        @if($hero->skills)
                            <h2>Умения</h2>
                            @foreach($hero->skills as $skill)
                                <div class="row">
                                    <div class="col-sm-1">
                                        <a href="#">
                                            <img class="card-img-top" src="{{ $skill->getFirstMediaUrl('image', 'thumb') }}" alt="">
                                        </a>
                                    </div>
                                    <a href="#">
                                        <h3>{{ $skill->name }}</h3>
                                    </a>
                                </div>
                            @endforeach
                        @endif



                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="row main-row ml-n1">
                    @foreach($heroesRandom as $hero)
                    <div class="main-card hero">
                        <a href="{{ route('hero.detail', ['id' => $hero->id]) }}">
                            <img class="card-img-top" src="{{ $hero->getFirstMediaUrl('image') }}" alt="">
                        </a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{ route('hero.detail', ['id' => $hero->id]) }}">
                                    <h3>{{ $hero->name }}</h3>
                                </a>
                            </h4>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
