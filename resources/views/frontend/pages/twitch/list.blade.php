<?php
/**
 * @var App\Models\Twitch\TwitchStreams[] $streams
 */
?>
@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="main-row mt-4">
            <div class="row">
                @foreach($streams as $key=>$stream)
                    <div class="col-md-4">
                        <div class="main-card">
                            <a href="{{ route('twitch.detail', ['id' => $stream->user_id]) }}">
                                <img class="card-img-top" src="{{ $stream->thumbnail_url }}" alt="">
                            </a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="{{ route('twitch.detail', ['id' => $stream->user_id]) }}"><h3>{{ $stream->user_name }}</h3></a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    @if(($key + 1) % 3 == 0)
            </div>
            <div class="row">
                @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
