<?php
/**
 * @var App\Models\Twitch\TwitchStreams $stream
 */
?>
@extends('frontend.layout')
@section('content')
    <div class="container content-black">
        <div class="row main-row mt-4">
            <h1>{{ $stream->title }}</h1>
            <div id="twitch-embed"></div>
            <!-- Load the Twitch embed script -->
            <script src="https://embed.twitch.tv/embed/v1.js"></script>

            <!-- Create a Twitch.Embed object that will render within the "twitch-embed" root element. -->
            <script type="text/javascript">
                new Twitch.Embed("twitch-embed", {
                    width: 1110,
                    height: 480,
                    channel: '{{ $stream->user_name }}',
                });
            </script>
        </div>
    </div>
@endsection
