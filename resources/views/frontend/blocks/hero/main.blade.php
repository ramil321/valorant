<?php
/**
 * @var \App\Models\Hero\Hero[] $heroes
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h2>Персонажи</h2>
    </div>
</div>
<div class="row">
@foreach($heroes as $key => $hero)
    <div class="col">
        <div  class="main-card hero min-hero-width grow main-hero-block">
            <a href="{{ route('hero.detail', ['id' => $hero->id]) }}">
                <div class="main-hero-image" style="
                background: url({{ $hero->getFirstMediaUrl('image') }});
                background-repeat: no-repeat;
                background-size: 400px;
                background-position-y: -30px;
                background-position-x: center;"></div>
            </a>
            <div class="card-body main-hero-body">
                <h4 class="card-title main-hero-title">
                    <a href="{{ route('hero.detail', ['id' => $hero->id]) }}">
                        {{ $hero->name }}
                    </a>
                </h4>
            </div>
        </div>
    </div>
@endforeach
</div>
