<?php
 /**
  * @var \App\Models\Twitch\TwitchStreams[] $twitchStreams
  */
?>
<div class="row">
    <div class="col-lg-12">
        <h2>Стримы</h2>
    </div>
</div>
<div class="row">
    @foreach($twitchStreams as $stream)
        <div class="col-lg-6">
            <div class="main-card">
                <a href="{{ route('twitch.detail', ['id' => $stream->user_id]) }}">
                    <img class="card-img-top" src="{{ $stream->thumbnail_url }}" alt="">
                </a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="{{ route('twitch.detail', ['id' => $stream->user_id]) }}">
                            {{ $stream->user_name }}
                        </a>
                    </h4>
                    <p class="card-text">{!! $stream->created_at !!}</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- /.row -->
