<?php
/**
 * @var \App\Models\News\News[] $news
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h2>Новости</h2>
    </div>
</div>
<div class="row">
    @foreach($news as $key => $item)
        <div class="col-lg-6">
            <div class="main-card">
                <a href="{{ route('news.detail', ['id' => $item->id]) }}">
                    <img class="card-img-top" src="{{ $item->getFirstMediaUrl('image', 'thumb') }}" alt="">
                </a>
                <div class="card-body">
                    <h4 class="card-title">
                        <a href="{{ route('news.detail', ['id' => $item->id]) }}">{{ $item->title }}</a>
                    </h4>
                </div>
            </div>
        </div>
    @endforeach
</div>
<!-- /.row -->
