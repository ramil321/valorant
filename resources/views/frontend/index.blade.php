@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="row main-row mt-4">
            @include('frontend.blocks.hero.main')
        </div>
        <div class="row main-row mt-4">
            <div class="col-md-6 main-block-content">
                @include('frontend.blocks.news.main')
            </div>
            <div class="col-md-6 main-block-content">
                @include('frontend.blocks.twitch.main')
            </div>
        </div>
    </div>
@endsection
